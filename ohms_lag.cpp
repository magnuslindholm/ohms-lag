
/*
created: 2018-12-17

notes:

*/
#include <iostream>

using namespace std;

int main()
{

    while (1)
    {
        int choice, number;
        double i=0, u=0, r1=0, r2=0, r3=0, r4=0, rtot=0;
        char cont;

        cout << endl;
        cout << "*******OHMS LAG********" << endl;
        cout << endl;
        cout << "Vad vill du berakna?" << endl;
        cout << endl;
        cout << "Spanning(U)         --  1" << endl;
        cout << "Strom(I)            --  2" << endl;
        cout << "Resistans(R)        --  3" << endl;
        cout << "Parallel Resistans  --  4" << endl;
        cin >> choice;
        cout << endl;

        switch (choice)
        {
            
        case 1:
        {
            cout << "***Spanning***" << endl;
            cout << "Mata in strom (Ampere): ";
            cin >> i;
            cout << endl << "Mata in resistans (Ohm): ";
            cin >> rtot;
            cout << endl;

            u = i * rtot;
            cout << "Spanningen ar: " << u << " volt." << endl;
            cout << endl;
            break;
        }
        case 2:
        {
            cout << "***Strom***" << endl;
            cout << "Mata in spanning (Volt): ";
            cin >> u;
            cout << endl;
            cout << "Mata in resistans (Ohm): ";
            cin >> rtot;

            i = u/rtot;
            cout << endl;
            cout << "Strommen ar: " << i << " ampere." << endl;
            cout << endl;
            break;
        }
        case 3:
        {
            cout << "***Resistans***" << endl;
            cout << "Mata in spanning(Volt): ";
            cin >> u;
            cout << endl;
            cout << "Mata in strom(Ampere): ";
            cin >> i;

            rtot = u/i;
            cout << endl;
            cout << "Resistansen ar: " << rtot << " ohm." << endl;
            cout << endl;
            break;
        }
        case 4:
        {
            cout << "Hur manga motstand handlar det om?: ";
            cin >> number;
            switch(number)
            {
                case 1:
                {
                    //fortsätt koda här
                    break;
                }
                case 2:
            }
            break;
        }
        }
        
        cout << endl;
        cout << "Vill du rakna mer? [y/n]: ";
        cin >> cont;
        if(cont == 'n')
        return 0;
        else
        cout << endl;
    }
    return 0;
}